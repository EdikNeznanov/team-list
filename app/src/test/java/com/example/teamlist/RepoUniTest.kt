package com.example.teamlist

import com.example.teamlist.model.Game
import com.example.teamlist.model.Player
import com.example.teamlist.model.Team
import com.example.teamlist.repository.Repo
import com.example.teamlist.viewmodel.*
import junit.framework.Assert.assertEquals
import org.mockito.junit.MockitoJUnit
import org.mockito.junit.MockitoRule
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.junit.runner.RunWith
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.quality.Strictness

@RunWith(MockitoJUnitRunner::class)
class RepoUnitTest {
    @Mock
    private val repositoryMock: Repo = mock(Repo::class.java)


    private val FakeGameDataVP: List<GameViewModel> = listOf(
        GameViewModel("Zalgiris", "Lietuvos Rytas", "Kovo 15"),
        GameViewModel("Zalgiris", "Lietuvos Rytas", "Geguzes 24"),
        GameViewModel("CSKA", "Toroto Raptors", "Rugsejo 6"),
        GameViewModel("Zalgiris", "CSKA", "Geguzes 15"),
        GameViewModel("Zalgiris", "Toroto Raptors", "Geguzes 11"),
        GameViewModel("Toroto Raptors", "Lietuvos Rytas", "Kovo 10"),
        GameViewModel("Zalgiris", "Lietuvos Rytas", "Rugsejo 15"),
        GameViewModel("CSKA", "Lietuvos Rytas", "Geguzes 15"),
        GameViewModel("Zalgiris", "Toroto Raptors", "Rugsejo 17"),
        GameViewModel("Zalgiris", "Lietuvos Rytas", "Kovo 26")
    )
    private val FakeGameData: List<Game> = listOf(
        Game("Zalgiris", "Lietuvos Rytas", "Kovo 15"),
        Game("Zalgiris", "Lietuvos Rytas", "Geguzes 24"),
        Game("CSKA", "Toroto Raptors", "Rugsejo 6"),
        Game("Zalgiris", "CSKA", "Geguzes 15"),
        Game("Zalgiris", "Toroto Raptors", "Geguzes 11"),
        Game("Toroto Raptors", "Lietuvos Rytas", "Kovo 10"),
        Game("Zalgiris", "Lietuvos Rytas", "Rugsejo 15"),
        Game("CSKA", "Lietuvos Rytas", "Geguzes 15"),
        Game("Zalgiris", "Toroto Raptors", "Rugsejo 17"),
        Game("Zalgiris", "Lietuvos Rytas", "Kovo 26")
    )
    private val FakeTeamData: List<Team> = listOf(
        Team("Toronto Raptors", "long story", "https://image.flaticon.com/icons/svg/185/185852.svg", 1),
        Team(
            "Toronto Velociraptors",
            "long story, long story, long story,long story long story long story long story long story long story long story long story long storylong storylong storylong story long story long story, long story,long story long story long story long story long story long story long story long story long storylong long story,long story long story long story long story long story long story long story long story long storylong long story,long story long story long story long story long story long story long story long story long storylong long story,long story long story long story long story long story long story long story long story long storylong",
            "https://www.trzcacak.rs/myfile/detail/11-117619_horse-icon.png",
            2
        ),
        Team(
            "Toronto Triceraptors",
            "long story",
            "https://previews.123rf.com/images/goodzone95/goodzone951611/goodzone95161100164/68628295-cat-icon-simple-design-on-a-white-background-vector-illustration.jpg",
            3
        ),
        Team(
            "Toronto Apatosaurus",
            "long story",
            "https://www.freepngimg.com/download/plant/71683-pubg-portable-telegram-pepe-frog-pol-jpeg.png",
            4
        )
    )
    private val FakeTeamDataVM: List<TeamViewModel> = listOf(
    TeamViewModel("Toronto Raptors", "long story", "https://image.flaticon.com/icons/svg/185/185852.svg", 1),
    TeamViewModel(
    "Toronto Velociraptors",
    "long story, long story, long story,long story long story long story long story long story long story long story long story long storylong storylong storylong story long story long story, long story,long story long story long story long story long story long story long story long story long storylong long story,long story long story long story long story long story long story long story long story long storylong long story,long story long story long story long story long story long story long story long story long storylong long story,long story long story long story long story long story long story long story long story long storylong",
    "https://www.trzcacak.rs/myfile/detail/11-117619_horse-icon.png",
    2
    ),
    TeamViewModel(
    "Toronto Triceraptors",
    "long story",
    "https://previews.123rf.com/images/goodzone95/goodzone951611/goodzone95161100164/68628295-cat-icon-simple-design-on-a-white-background-vector-illustration.jpg",
    3
    ),
    TeamViewModel(
    "Toronto Apatosaurus",
    "long story",
    "https://www.freepngimg.com/download/plant/71683-pubg-portable-telegram-pepe-frog-pol-jpeg.png",
    4
    )
    )
    private val FakePlayersData: List<Player> = listOf(Player(
        "Jonas Valanciunas",
        25,
        220,
        80,
        "Profesionalo karjerą pradėjo 2008–2009 m. sezone Vilniaus „Perlo„ komandoje, kuri rungtyniavo Nacionalinėje krepšinio lygoje (NKL). 2009–2010 m. sezone kartu su Vilniaus „Perlo“ komanda debiutavo aukščiausioje Lietuvos krepšinio lygoje (LKL) ir buvo vienas komandos lyderių.\n" +
                "\n" +
                "2010 m. sausio 17 d. jaunasis Lietuvos krepšinio talentas perėjo į Vilniaus Lietuvos ryto komandą, [1] su kuria tapo LKF ir LKL čempionu.",
        "image",
        "center",
        21,
        4
    ))
    private val FakePlayersDataVM: List<ListPlayerViewModel> = listOf(ListPlayerViewModel("Jonas Valanciunas", "center", 21))

    private val FakePlayerData: Player = Player(
        "Jonas Valanciunas",
        25,
        220,
        80,
        "Profesionalo karjerą pradėjo 2008–2009 m. sezone Vilniaus „Perlo„ komandoje, kuri rungtyniavo Nacionalinėje krepšinio lygoje (NKL). 2009–2010 m. sezone kartu su Vilniaus „Perlo“ komanda debiutavo aukščiausioje Lietuvos krepšinio lygoje (LKL) ir buvo vienas komandos lyderių.\n" +
                "\n" +
                "2010 m. sausio 17 d. jaunasis Lietuvos krepšinio talentas perėjo į Vilniaus Lietuvos ryto komandą, [1] su kuria tapo LKF ir LKL čempionu.",
        "image",
        "center",
        21,
        4
    )

    private var FakePlayerDataVM: PlayerViewModel = PlayerViewModel(21, repositoryMock)

    @get:Rule
    val mrule: MockitoRule = MockitoJUnit.rule().strictness(Strictness.LENIENT)

    @Test
    fun initializeDataFromRepository_onManyGamesViewModel() {
        `when`(repositoryMock.getGamesList()).thenReturn(FakeGameData)
        val gamesViewModel = ManyGamesViewModel(repositoryMock)
        assertEquals(gamesViewModel.getGamesForList(), FakeGameDataVP)
    }

    @Test
    fun initializeDataFromRepository_onManyTeamsViewModel() {
        `when`(repositoryMock.getTeamsList()).thenReturn(FakeTeamData)
        val teamsViewModel = ManyTeamsViewModel(repositoryMock)
        assertEquals(teamsViewModel.getTeamsForList(), FakeTeamDataVM)
    }

    @Test
    fun initializeDataFromRepository_onManyPlayersViewModel(){
        `when`(repositoryMock.getPlayersDataByTeamId(1)).thenReturn(FakePlayersData)
        val playersViewModel = ManyPlayersViewModel(repositoryMock)
        assertEquals(playersViewModel.getPlayersForList(1), FakePlayersDataVM)
    }
    @Test
    fun initializeDataFromRepository_onListPlayersViewModel(){
//    FakePlayerDataVM.playerName=
        `when`(repositoryMock.getPlayerDataById(21)).thenReturn(FakePlayerData)
        val playerViewModel = PlayerViewModel(21, repositoryMock)
        assertEquals(playerViewModel, "actual name")
    }
}