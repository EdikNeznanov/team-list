package com.example.teamlist

import android.app.Activity
import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import com.example.teamlist.dagger.ApplicationComponent
import com.example.teamlist.dagger.DaggerApplicationComponent
import com.example.teamlist.database.*
import com.example.teamlist.networking.Downloader
import com.example.teamlist.networking.DownloaderInterface
import com.example.teamlist.repository.TeamRepo
import com.example.teamlist.repository.TeamRepository
import com.example.teamlist.sharedPrefs.SharedPreferencesHandler
import com.example.teamlist.viewmodel.ManyTeamsViewModel
import dagger.Module
import dagger.Provides
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import javax.inject.Inject

//@Module
class NBAApplication : Application(), HasActivityInjector {

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Activity>

    override fun activityInjector(): AndroidInjector<Activity> {
        return dispatchingAndroidInjector
    }

    override fun onCreate() {
        super.onCreate()
        DaggerApplicationComponent.builder()
            .application(this)
            .build()
            .inject(this)
    }

    //@Provides
    fun provideDatabase(): BasketballDB {
        return BasketballDB.getInstance(applicationContext)
    }


    //@Provides
    fun provideTeamRepository(mPrefs: SharedPreferencesHandler, database: BasketballDB): TeamRepo {
        return TeamRepository.getInstance(mPrefs, database)
    }

    fun provideDownloader(): DownloaderInterface {
        return Downloader.getInstance()
    }


}