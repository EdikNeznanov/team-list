package com.example.teamlist.converters

import com.example.teamlist.database.GameEntity
import com.example.teamlist.database.PlayerEntity
import com.example.teamlist.database.TeamEntity
import com.example.teamlist.model.Game
import com.example.teamlist.model.Player
import com.example.teamlist.model.Team

class DbToModel {
    fun convertTeamsDbToModels(teamEntity: TeamEntity): Team{
        return Team(teamEntity.teamName,
            teamEntity.teamDescription,
            teamEntity.teamPhoto,
            teamEntity.teamId)
    }
    fun convertGamesDbToModels(gameEntity: GameEntity): Game{
        return Game(gameEntity.homeTeamName, gameEntity.awayTeamName, gameEntity.gameDate)
    }

    fun convertPlayersDbToModels(pe: PlayerEntity): Player{
        return Player(pe.playerName!!, pe.playerAge!!, pe.playerHeight!!, pe.playerWeight!!, pe.playerDescription!!, pe.playerPhoto!!, pe.playerPosition!!, pe.playerId!!, pe.teamId!!)
    }
}