package com.example.teamlist.converters

import com.example.teamlist.database.GameEntity
import com.example.teamlist.database.PlayerEntity
import com.example.teamlist.database.TeamEntity
import com.example.teamlist.model.Game
import com.example.teamlist.model.Player
import com.example.teamlist.model.Team

class ModelToDb(){
    fun convertTeamModelToEntity(team: Team): TeamEntity{
        return TeamEntity(null, team.teamId, team.teamName, team.teamStory, team.teamLogoURL)
    }
    fun convertGameModelToEntity(game: Game): GameEntity{
        return GameEntity(null, null, game.teamAway, game.teamHome, game.date)
    }
    fun convertPlayerModelToEntity(player: Player): PlayerEntity {
        return PlayerEntity(null, player.playerId, player.teamId, player.name, player.age, player.height, player.weight, player.story, player.image, player.position)
    }
}