package com.example.teamlist.networking

import com.example.teamlist.model.TeamsDTO
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path


interface BasketballTeamsApi {
    @GET("lookup_all_teams.php/?id=4387") //url direction here
    fun getTeamWithID(): Call<TeamsDTO> //callback
}