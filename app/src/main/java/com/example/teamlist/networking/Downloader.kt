package com.example.teamlist.networking

import android.os.AsyncTask
import android.util.Log
import androidx.annotation.NonNull
import com.example.teamlist.model.GamesDTO
import com.example.teamlist.model.PlayersDTO
import com.example.teamlist.model.Team
import com.example.teamlist.model.TeamsDTO
import com.example.teamlist.repository.Repo
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.BufferedReader
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.URL
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class Downloader : DownloaderInterface
{

    private val retrofit: Retrofit

    init{
        retrofit = Retrofit.Builder()
            .baseUrl("https://www.thesportsdb.com/api/v1/json/1/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    companion object {
        private var instance: Downloader? = null

        fun getInstance(): Downloader {
            if (instance == null) {
                instance = Downloader()
            }

            return instance!!
        }
    }

    fun getBasketballTeamsApi(): BasketballTeamsApi{
        return retrofit.create(BasketballTeamsApi::class.java)
    }

    fun getBasketballPlayerApi(): BasketballPlayerApi{
        return retrofit.create(BasketballPlayerApi::class.java)
    }

    fun getBasketballGamesApi(): BasketballGamesApi{
        return retrofit.create(BasketballGamesApi::class.java)
    }
    override fun getBasketballGames(teamID: Int, listener: Repo.Listener<GamesDTO>) {
        getBasketballGamesApi().getGamesWithID(teamID).enqueue(object : Callback<GamesDTO> {
            override fun onResponse(@NonNull call: Call<GamesDTO>, @NonNull response: Response<GamesDTO>) {
                listener.onResult(response.body()!!)
            }

            override fun onFailure(@NonNull call: Call<GamesDTO>, @NonNull t: Throwable) {

                t.printStackTrace()
            }
        })
    }

    override fun getBasketballTeams(listener: Repo.Listener<TeamsDTO>) {
        getBasketballTeamsApi().getTeamWithID().enqueue(object : Callback<TeamsDTO> {
            override fun onResponse(@NonNull call: Call<TeamsDTO>, @NonNull response: Response<TeamsDTO>) {
                listener.onResult(response.body()!!)
            }

            override fun onFailure(@NonNull call: Call<TeamsDTO>, @NonNull t: Throwable) {
                t.printStackTrace()
            }
        }
        )    }

    override fun getBasketballPlayers(teamID: Int, listener: Repo.Listener<PlayersDTO>) {
        getBasketballPlayerApi().getPlayersWithID(teamID).enqueue(object : Callback<PlayersDTO> {
            override fun onResponse(@NonNull call: Call<PlayersDTO>, @NonNull response: Response<PlayersDTO>) {
                    listener.onResult(response.body()!!)
            }

            override fun onFailure(@NonNull call: Call<PlayersDTO>, @NonNull t: Throwable) {

                t.printStackTrace()
            }
        })
    }
}