package com.example.teamlist.networking

import com.example.teamlist.model.PlayerDTO
import com.example.teamlist.model.PlayersDTO
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface BasketballPlayerApi {
    @GET("lookup_all_players.php") //url direction here
    fun getPlayersWithID(@Query("id") id: Int): Call<PlayersDTO> //callback
}