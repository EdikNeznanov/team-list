package com.example.teamlist.networking

import com.example.teamlist.model.GamesDTO
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface BasketballGamesApi {
    @GET("eventslast.php") //url direction here
    fun getGamesWithID(@Query("id")id: Int): Call<GamesDTO> //callback
}