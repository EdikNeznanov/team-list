package com.example.teamlist.networking

import com.example.teamlist.model.GamesDTO
import com.example.teamlist.model.PlayersDTO
import com.example.teamlist.model.TeamsDTO
import com.example.teamlist.repository.Repo

interface DownloaderInterface: Repo {
fun getBasketballGames(teamID: Int, listener: Repo.Listener<GamesDTO>)
fun getBasketballTeams(listener: Repo.Listener<TeamsDTO>)
fun getBasketballPlayers(teamID: Int, listener: Repo.Listener<PlayersDTO>)
}