package com.example.teamlist.dagger

import android.content.Context
import android.content.SharedPreferences
import com.example.teamlist.database.BasketballDB
import com.example.teamlist.repository.TeamRepo
import com.example.teamlist.repository.TeamRepository
import com.example.teamlist.sharedPrefs.SharedPreferencesHandler
import com.example.teamlist.viewmodel.ManyTeamsViewModel
import dagger.Module
import dagger.Provides

@Module
class MainActivityModule {


    @Provides
    fun provideTeamRepository(mPrefs: SharedPreferencesHandler, database: BasketballDB): TeamRepo {
        return TeamRepository.getInstance(mPrefs, database)
    }

    @Provides
    fun provideTeamsVM(mRepository: TeamRepo): ManyTeamsViewModel {
        return ManyTeamsViewModel(mRepository)
    }
}