package com.example.teamlist.dagger

import android.app.Application
import com.example.teamlist.NBAApplication
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(AndroidInjectionModule::class, ApplicationModule::class, ActivityModule::class))
interface ApplicationComponent {
    fun inject(application: NBAApplication)

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: Application): Builder
        fun applicationModule(applicationModule: ApplicationModule)
        fun build(): ApplicationComponent
    }
}