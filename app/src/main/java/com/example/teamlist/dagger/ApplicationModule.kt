package com.example.teamlist.dagger

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import com.example.teamlist.database.BasketballDB
import com.example.teamlist.sharedPrefs.SharedPreferencesHandler
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module(includes= arrayOf(MainActivityModule::class))
class ApplicationModule
{

    @Provides
    @Singleton
    internal fun provideContext(application: Application): Context {
        return application
    }

    @Provides
    @Singleton
    fun provideSharedPrefs(context: Context): SharedPreferencesHandler {
        return SharedPreferencesHandler.getInstance(context.getSharedPreferences("something", Context.MODE_PRIVATE))
    }

    @Provides
    @Singleton
    fun provideDatabase(context: Context): BasketballDB {
        return BasketballDB.getInstance(context)
    }

}