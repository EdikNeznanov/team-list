package com.example.teamlist.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.ViewPager
import com.example.teamlist.R
import com.example.teamlist.adapter.GamesPlayersVPAdapter
import com.example.teamlist.databinding.ActivityGamesTeamsBinding
import com.google.android.material.tabs.TabLayout
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_games_teams.*
import java.util.ArrayList

class GamesPlayersActivity : AppCompatActivity() {

    var mFragment = ArrayList<Fragment>()
    lateinit var binder: ActivityGamesTeamsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binder = DataBindingUtil.setContentView(this, R.layout.activity_games_teams)
        setSupportActionBar(gamesTeamsToolbar)
        super.getSupportActionBar()?.setDisplayHomeAsUpEnabled(true)
        super.getSupportActionBar()?.setDisplayShowHomeEnabled(true)
        super.getSupportActionBar()?.setHomeAsUpIndicator(R.drawable.ic_chevron_left_white_48dp)
        val teamId = intent?.getIntExtra("teamID", 0)!!
        val teamName = intent?.getStringExtra("teamName")
        Picasso.get().load(intent?.getStringExtra("teamLogo")).into(binder.imageViewCollapsing)
        if (teamName!=null){
            super.getSupportActionBar()?.title = teamName
            binder.collapsingToolbarLayout.title = teamName
            binder.collapsingToolbarLayout.setExpandedTitleColor(resources.getColor(android.R.color.white))

        }
        mFragment.add(GamesFragment.newInstance(teamId))
        mFragment.add(PlayersFragment.newInstance(teamId))
        val mViewPager: ViewPager = binder.viewPager
        mViewPager.setAdapter(GamesPlayersVPAdapter(supportFragmentManager, mFragment))
        val mTabLayout: TabLayout = binder.tlTabsContainer
        mTabLayout.setupWithViewPager(mViewPager)
    }
}
