package com.example.teamlist

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Html
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.teamlist.adapter.TeamsAdapter
import com.example.teamlist.databinding.ActivityMainBinding
import com.example.teamlist.repository.TeamRepository
import com.example.teamlist.view.GamesPlayersActivity
import com.example.teamlist.viewmodel.ManyTeamsViewModel
import com.example.teamlist.viewmodel.TeamViewModel
import dagger.android.AndroidInjection
import javax.inject.Inject

class MainActivity : AppCompatActivity(), TeamsAdapter.TeamsAdapterOnClickHandler, ManyTeamsViewModel.ManyTeamsVMInterface {

    @Inject
    lateinit var teamsViewModel: ManyTeamsViewModel
    lateinit var binder: ActivityMainBinding
    private lateinit var recyclerAdapter: TeamsAdapter
    lateinit var mRecycler: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        binder = DataBindingUtil.setContentView(this, R.layout.activity_main)
        super.getSupportActionBar()?.setTitle(Html.fromHtml("<font color='#ffffff'>NBA </font>"))
        mRecycler = binder.teamListRecycler
        val linearLayoutManager = LinearLayoutManager(this)
        mRecycler.layoutManager = linearLayoutManager
        recyclerAdapter = TeamsAdapter(ArrayList(), this)
        mRecycler.adapter = recyclerAdapter
        val app = application as NBAApplication
        //teamsViewModel = ManyTeamsViewModel(TeamRepository.getInstance(app.provideSharedPrefs(), app.provideDatabase()))
//        val tVMComponent: TeamsVMComponent = DaggerTeamsVMComponent.create()
//        tVMComponent.inject(this)

        teamsViewModel.getTeamsForList(this@MainActivity)

    }

    override fun onDataReady(teamViewModel: List<TeamViewModel>) {
        recyclerAdapter.setData(teamViewModel)
        recyclerAdapter.notifyDataSetChanged()
    }

    override fun onClick(position: Int, teamId: Int, teamName: String, teamLogo: String) {
    val intent = Intent(this, GamesPlayersActivity::class.java)
        intent.putExtra("teamID", teamId)
        intent.putExtra("teamName", teamName)
        intent.putExtra("teamLogo", teamLogo)
    startActivity(intent)
    }
}
