package com.example.teamlist.view

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.teamlist.NBAApplication

import com.example.teamlist.R
import com.example.teamlist.adapter.PlayersAdapter
import com.example.teamlist.databinding.FragmentPlayersBinding
import com.example.teamlist.repository.PlayerRepository
import com.example.teamlist.viewmodel.ListPlayerViewModel
import com.example.teamlist.viewmodel.ManyPlayersViewModel


class PlayersFragment : Fragment(), PlayersAdapter.RecyclerViewAdapterOnClickHandler, ManyPlayersViewModel.ManyPlayersVMInterface {



    private lateinit var playersViewModel: ManyPlayersViewModel
    lateinit var binder : FragmentPlayersBinding
    private lateinit var recyclerAdapter: PlayersAdapter
    lateinit var mRecycler: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val app = activity?.application as NBAApplication
        //playersViewModel = ManyPlayersViewModel(PlayerRepository.getInstance(app.provideSharedPrefs(), app.provideDatabase()))
        playersViewModel.getPlayersForList(arguments?.getInt("TEAM_ID")!!, this)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binder = DataBindingUtil.inflate(inflater, R.layout.fragment_players, container, false)

        mRecycler = binder.playersRecycler
        val linearLayoutManager = LinearLayoutManager(activity)
        mRecycler.layoutManager = linearLayoutManager
        recyclerAdapter = PlayersAdapter(ArrayList(), this)
        mRecycler.adapter = recyclerAdapter


        return binder.root    }

    override fun onDataReady(listPlayerViewModel: List<ListPlayerViewModel>) {
        recyclerAdapter.setData(listPlayerViewModel)
        recyclerAdapter.notifyDataSetChanged()
    }
    override fun onClick(position: Int, playerID: Int, playerName: String) {
    val intent = Intent(activity, PlayerActivity::class.java)
        intent.putExtra("playerID", playerID)
        intent.putExtra("teamID", arguments?.getInt("TEAM_ID")!!)
        intent.putExtra("playerName", playerName)
    startActivity(intent)
    }

    companion object {
        @JvmStatic
        fun newInstance(teamID: Int): PlayersFragment {
            val args = Bundle()
            args.putInt("TEAM_ID", teamID)
            val fragment = PlayersFragment()
            fragment.arguments = args
            return  fragment
        }

    }
}
