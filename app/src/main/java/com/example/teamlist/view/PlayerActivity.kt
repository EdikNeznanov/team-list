package com.example.teamlist.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.ActionBar
import androidx.databinding.DataBindingUtil
import com.example.teamlist.NBAApplication
import com.example.teamlist.R
import com.example.teamlist.databinding.ActivityPlayerBinding
import com.example.teamlist.model.Player
import com.example.teamlist.repository.PlayerRepository
import com.example.teamlist.viewmodel.PlayerViewModel
import com.google.android.material.appbar.CollapsingToolbarLayout
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_player.*

class PlayerActivity : AppCompatActivity(), PlayerViewModel.PlayerVMListener {

    lateinit var binder : ActivityPlayerBinding
    lateinit var playerVm: PlayerViewModel
    lateinit var actionbar: ActionBar
    lateinit var collapsingToolbar: CollapsingToolbarLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binder = DataBindingUtil.setContentView(this, R.layout.activity_player)
        setSupportActionBar(toolbar)
        val playerID = intent.extras?.getInt("playerID", 0)!!
        val teamID = intent.extras?.getInt("teamID", 0)!!
        collapsingToolbar  = (collapsingToolbarLayout)
        val app = application as NBAApplication
       // playerVm = PlayerViewModel(teamID, playerID, PlayerRepository.getInstance(app.provideSharedPrefs(), app.provideDatabase()))
        playerVm.getPlayersData(this)
        binder.playerViewModel = playerVm
    }

    override fun onDataReady(player: Player) {
        if (player.playerId != 0) {
            super.getSupportActionBar()?.setDisplayHomeAsUpEnabled(true)
            super.getSupportActionBar()?.setDisplayShowHomeEnabled(true)
            super.getSupportActionBar()?.setHomeAsUpIndicator(R.drawable.ic_chevron_left_white_48dp)
            collapsingToolbar.title = player.name
            collapsingToolbar.setCollapsedTitleTextColor(resources.getColor(android.R.color.white))
            collapsingToolbar.setExpandedTitleColor(resources.getColor(android.R.color.white))
            Picasso.get().load(player.image).into(binder.imageViewCollapsing)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home){
            onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }
}
