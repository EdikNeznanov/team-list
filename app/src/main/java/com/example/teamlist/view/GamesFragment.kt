package com.example.teamlist.view


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.teamlist.NBAApplication

import com.example.teamlist.R
import com.example.teamlist.adapter.GamesAdapter
import com.example.teamlist.databinding.FragmentGamesBinding
import com.example.teamlist.repository.GameRepository
import com.example.teamlist.viewmodel.GameViewModel
import com.example.teamlist.viewmodel.ManyGamesViewModel


class GamesFragment : Fragment() {

    private lateinit var teamsViewModel: ManyGamesViewModel
    lateinit var binder: FragmentGamesBinding
    private lateinit var recyclerAdapter: GamesAdapter
    lateinit var mRecycler: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val app = activity?.application as NBAApplication
        //teamsViewModel = ManyGamesViewModel(GameRepository.getInstance(app.provideSharedPrefs(), app.provideDatabase()))
        teamsViewModel.gamesListViewModel.observe(this, Observer { gamesList -> onDataReady(gamesList) })
        teamsViewModel.getGamesForList(arguments?.getInt("TEAM_ID")!!)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binder = DataBindingUtil.inflate(inflater, R.layout.fragment_games, container, false)
        mRecycler = binder.gamesRecycler
        val linearLayoutManager = LinearLayoutManager(activity)
        mRecycler.layoutManager = linearLayoutManager
        recyclerAdapter = GamesAdapter(ArrayList())
        mRecycler.adapter = recyclerAdapter
        return binder.root
    }

    companion object {
        @JvmStatic
        fun newInstance(teamID: Int): GamesFragment {
            val args = Bundle()
            args.putInt("TEAM_ID", teamID)
            val fragment = GamesFragment()
            fragment.arguments = args
            return fragment
        }
    }

    fun onDataReady(gamesViewModel: List<GameViewModel>) {
        recyclerAdapter.setData(gamesViewModel)
        recyclerAdapter.notifyDataSetChanged()
    }
}
