package com.example.teamlist.repository

import com.example.teamlist.model.Player

interface PlayerRepo: Repo {
    fun getPlayerDataById(teamId: Int, playerId: Int, listener: Repo.Listener<Player>)
    fun getPlayersDataByTeamId(id: Int, listener: Repo.Listener<List<Player>>)
}