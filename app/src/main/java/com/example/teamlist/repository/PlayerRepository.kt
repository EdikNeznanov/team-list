package com.example.teamlist.repository

import android.util.Log
import androidx.annotation.NonNull
import com.example.teamlist.converters.DbToModel
import com.example.teamlist.converters.ModelToDb
import com.example.teamlist.database.*
import com.example.teamlist.model.Player
import com.example.teamlist.model.PlayersDTO
import com.example.teamlist.networking.Downloader
import com.example.teamlist.sharedPrefs.SharedPreferencesHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.Period
import java.time.ZoneId
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class PlayerRepository @Inject constructor(val sharedPrefsHandler: SharedPreferencesHandler, val playerDB: BasketballDB) : PlayerRepo {
    val converterToDb = ModelToDb()
    val converterFromModel = DbToModel()
    val job = Job()
    val ioScope = CoroutineScope(Dispatchers.IO + job)
    val uiScope = CoroutineScope(Dispatchers.Main + job)

    companion object {
        private var instance: PlayerRepository? = null

        fun getInstance(sharedPrefsHandler: SharedPreferencesHandler, playerDB: BasketballDB): PlayerRepository {
            if (instance == null) {
                instance = PlayerRepository(sharedPrefsHandler, playerDB)
            }

            return instance!!
        }
    }


    override fun getPlayersDataByTeamId(id: Int, listener: Repo.Listener<List<Player>>) {
        val mDownloader = Downloader.getInstance()
        val sharedPrefs = sharedPrefsHandler.sharedPrefs
        val renewTime = sharedPrefs.getLong(ApiRefreshRates.Player.ID+id, 0)
        if (ApiRefreshRates.Player.renewTimeMS <= System.currentTimeMillis() - renewTime){
            Log.e("tag", "internet")
            sharedPrefs.edit().putLong(ApiRefreshRates.Player.ID + id, System.currentTimeMillis()).apply()
            mDownloader.getBasketballPlayers(id, object:Repo.Listener<PlayersDTO>{
            override fun <T> onResult(data: T) {
                data as PlayersDTO
                val teamPlayersModel: List<Player> = data.player.map { player -> Player(player.strPlayer, convertDateofBirthToAge(player.dateBorn), 78, 180, player.strDescriptionEN, player.strThumb, player.strPosition, player.idPlayer, player.idTeam)}
                ioScope.launch {
                for(player in teamPlayersModel){
                    playerDB.playerDao().addPlayer(converterToDb.convertPlayerModelToEntity(player))
                }
                }
                listener.onResult(teamPlayersModel)
            }
        })
        } else{
            Log.e("tag", "got here to take players from db")
            val playes = playerDB.playerDao().getPlayers(id)
                    listener.onResult(playes.map { player-> converterFromModel.convertPlayersDbToModels(player)})
        }
    }

    override fun getPlayerDataById(teamId: Int, playerId: Int, listener: Repo.Listener<Player>) {
        val mDownloader = Downloader.getInstance()
        val sharedPrefs = sharedPrefsHandler.sharedPrefs
        val renewTime = sharedPrefs.getLong(ApiRefreshRates.Player.ID + playerId, 0)
        if (ApiRefreshRates.Player.renewTimeMS <= System.currentTimeMillis() - renewTime) {
            sharedPrefs.edit().putLong(ApiRefreshRates.Player.ID + playerId, System.currentTimeMillis()).apply()
            mDownloader.getBasketballPlayers(teamId, object : Repo.Listener<PlayersDTO> {
                override fun <T> onResult(data: T) {
                    data as PlayersDTO
                    val teamPlayersModel: List<Player> = data.player.map { player ->
                        Player(
                            player.strPlayer,
                            convertDateofBirthToAge(player.dateBorn),
                            78,
                            180,
                            player.strDescriptionEN,
                            player.strThumb,
                            player.strPosition,
                            player.idPlayer,
                            player.idTeam
                        )
                    }
                    for (player in teamPlayersModel) {
                        if (player.playerId == playerId) {
                            listener.onResult(player)
                        }
                    }
                }
            })
        } else {
            Log.e("tag", "got player from db")
                   listener.onResult(converterFromModel.convertPlayersDbToModels(playerDB.playerDao().getPlayerById(playerId)))
        }
    }

    fun convertDateofBirthToAge(DoB: String): Int {
        val now: LocalDate = LocalDate.now()
        val date = SimpleDateFormat("yyyy-MM-dd").parse(DoB).toInstant().atZone(ZoneId.systemDefault()).toLocalDate()
        return Period.between(date, now).years
    }
}