package com.example.teamlist.repository


interface Repo{

    interface Listener<T> {
        fun <T> onResult(data: T)
    }
}