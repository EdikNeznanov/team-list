package com.example.teamlist.repository

import android.util.Log
import com.example.teamlist.converters.DbToModel
import com.example.teamlist.converters.ModelToDb
import com.example.teamlist.database.ApiRefreshRates
import com.example.teamlist.database.BasketballDB
import com.example.teamlist.model.Game
import com.example.teamlist.model.GamesDTO
import com.example.teamlist.networking.Downloader
import com.example.teamlist.sharedPrefs.SharedPreferencesHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class GameRepository @Inject constructor(val sharedPrefsHandler: SharedPreferencesHandler, val gameDB: BasketballDB) : GameRepo {
    val converterToDb = ModelToDb()
    val converterFromModel = DbToModel()
    val job = Job()
    val ioScope = CoroutineScope(Dispatchers.IO + job)
    companion object {
        private var instance: GameRepository? = null

        fun getInstance(sharedPrefsHandler: SharedPreferencesHandler, gameDB: BasketballDB): GameRepository {
            if (instance == null) {
                instance = GameRepository(sharedPrefsHandler, gameDB)
            }

            return instance!!
        }
    }

    override fun getGamesList(teamID: Int, listener: Repo.Listener<List<Game>>) {
        val mDownloader = Downloader.getInstance()
        val sharedPrefs = sharedPrefsHandler.sharedPrefs
        val renewTime = sharedPrefs.getLong(ApiRefreshRates.Game.ID + teamID.toString(), 0)
        if (ApiRefreshRates.Game.renewTimeMS <= System.currentTimeMillis() - renewTime) {
            sharedPrefs.edit().putLong(ApiRefreshRates.Game.ID + teamID.toString(), System.currentTimeMillis()).apply()
            mDownloader.getBasketballGames(teamID, object : Repo.Listener<GamesDTO> {
                override fun <T> onResult(data: T) {
                    data as GamesDTO
                    val gameModel = data.games.map { game -> Game(game.strHomeTeam, game.strAwayTeam, game.dateEvent) }
                ioScope.launch {
                    for (game in gameModel) {
                        gameDB.gameDao().deleteGame(converterToDb.convertGameModelToEntity(game))
                        gameDB.gameDao().addGame(converterToDb.convertGameModelToEntity(game))//, teamID
                    }
                }
                    listener.onResult(gameModel)
                }
            })
        } else {
            Log.e("tag", "getting data from db")
            listener.onResult(gameDB.gameDao().getGames().map { game -> converterFromModel.convertGamesDbToModels(game) })
        }
    }
}