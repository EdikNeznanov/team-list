package com.example.teamlist.repository

import android.util.Log
import com.example.teamlist.converters.DbToModel
import com.example.teamlist.converters.ModelToDb
import com.example.teamlist.database.ApiRefreshRates
import com.example.teamlist.database.BasketballDB
import com.example.teamlist.model.Team
import com.example.teamlist.model.TeamsDTO
import com.example.teamlist.networking.Downloader
import com.example.teamlist.sharedPrefs.SharedPreferencesHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class TeamRepository  constructor(val sharedPrefsHandler: SharedPreferencesHandler, val teamDB: BasketballDB) : TeamRepo {
    val converterToDb = ModelToDb()
    val converterFromModel = DbToModel()
    val job = Job()
    val ioScope = CoroutineScope(Dispatchers.IO + job)

    //TODO: how to deal with singleton companion objects
//TODO: each repository requires its own module/component?
    companion object {
        private var instance: TeamRepository? = null

        fun getInstance(sharedPrefs: SharedPreferencesHandler, teamDB: BasketballDB): TeamRepository {
            if (instance == null) {
                instance = TeamRepository(sharedPrefs, teamDB)
            }

            return instance!!
        }
    }

    override fun getTeamsList(listener: Repo.Listener<List<Team>>) {
        val mDownloader = Downloader.getInstance()
        val sharedPrefs = sharedPrefsHandler.sharedPrefs
        val renewTime = sharedPrefs.getLong(ApiRefreshRates.Team.ID, 0)
        if (ApiRefreshRates.Team.renewTimeMS <= System.currentTimeMillis() - renewTime) {
            sharedPrefs.edit().putLong(ApiRefreshRates.Team.ID, System.currentTimeMillis()).apply()
            Log.e("tag", "downloading data from api")
            mDownloader.getBasketballTeams(object : Repo.Listener<TeamsDTO> {
                override fun <T> onResult(data: T) {
                    data as TeamsDTO
                    val gameModel = data.teams.map { team ->
                        Team(
                            team.strTeam,
                            team.strDescriptionEN,
                            team.strTeamLogo,
                            team.idTeam
                        )
                    }
                    ioScope.launch {
                        for (team in gameModel) {
                            teamDB.teamDao().addTeam(converterToDb.convertTeamModelToEntity(team))
                        }
                    }
                    listener.onResult(gameModel)
                }
            })
        } else {
            Log.e("tag", "getting data from db")
                    listener.onResult(teamDB.teamDao().getTeams().map { team -> converterFromModel.convertTeamsDbToModels(team)} )


        }
    }
}