package com.example.teamlist.repository

import com.example.teamlist.model.Game

interface GameRepo : Repo {
    fun getGamesList(teamID: Int, listener: Repo.Listener<List<Game>>)
}