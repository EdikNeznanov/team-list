package com.example.teamlist.repository

import com.example.teamlist.model.Team

interface TeamRepo: Repo {
    fun getTeamsList(listener: Repo.Listener<List<Team>>)

}