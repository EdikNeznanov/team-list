package com.example.teamlist.viewmodel

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.squareup.picasso.Picasso

data class TeamViewModel(val teamName: String, val teamDesc: String, val logo: String, val teamId: Int)