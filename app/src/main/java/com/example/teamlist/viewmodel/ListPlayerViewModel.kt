package com.example.teamlist.viewmodel

data class ListPlayerViewModel(val playerName: String, val position: String, val playerId: Int, val photoURL: String) {
}