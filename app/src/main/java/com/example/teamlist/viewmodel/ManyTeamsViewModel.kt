package com.example.teamlist.viewmodel

import com.example.teamlist.model.Team
import com.example.teamlist.repository.Repo
import com.example.teamlist.repository.TeamRepo
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import javax.inject.Inject

class ManyTeamsViewModel @Inject constructor(
    private val mRepository: TeamRepo
) {
    val job = Job()
    val ioScope = CoroutineScope(Dispatchers.IO + job)
    val uiScope = CoroutineScope(Dispatchers.Main + job)

    fun getTeamsForList(listener: ManyTeamsVMInterface){
        ioScope.launch {
            mRepository.getTeamsList(object : Repo.Listener<List<Team>> {
                lateinit var initialData: List<Team>
                override fun <T> onResult(data: T) {
                    initialData = data as List<Team>
                    var listToDisplay: ArrayList<TeamViewModel> = ArrayList()
                    if (initialData != null) {
                        for (data in initialData) {
                            listToDisplay.add(
                                TeamViewModel(
                                    data.teamName,
                                    data.teamStory,
                                    data.teamLogoURL,
                                    data.teamId
                                )
                            )
                        }
                    }
                    uiScope.launch { listener.onDataReady(listToDisplay)}
                }
            })
        }
    }

interface ManyTeamsVMInterface{
    fun onDataReady(teamViewModel: List<TeamViewModel>)
}
}