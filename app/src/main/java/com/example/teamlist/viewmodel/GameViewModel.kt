package com.example.teamlist.viewmodel

data class GameViewModel(val team1: String, val team2: String, val date: String) {
}