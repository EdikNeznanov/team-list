package com.example.teamlist.viewmodel

import androidx.lifecycle.MutableLiveData
import com.example.teamlist.model.Game
import com.example.teamlist.repository.GameRepo
import com.example.teamlist.repository.Repo
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class ManyGamesViewModel(
    private val mRepository: GameRepo
){
    val job = Job()
    val ioScope = CoroutineScope(Dispatchers.IO + job)
    val uiScope = CoroutineScope(Dispatchers.Main + job)
    var gamesListViewModel: MutableLiveData<List<GameViewModel>> = MutableLiveData()

    fun getGamesForList(teamId: Int){
        ioScope.launch {
            mRepository.getGamesList(teamId, object : Repo.Listener<List<Game>> {
                override fun <T> onResult(data: T) {
                    var initialData = data as List<Game>
                    var listToDisplay: ArrayList<GameViewModel> = ArrayList()
                    for (data in initialData) {
                        listToDisplay.add(GameViewModel(data.teamHome, data.teamAway, data.date))
                    }
                    uiScope.launch {
                        gamesListViewModel.value = listToDisplay
                    }
                }
            })
        }
    }
}