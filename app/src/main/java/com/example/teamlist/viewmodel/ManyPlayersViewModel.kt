package com.example.teamlist.viewmodel

import com.example.teamlist.model.Player
import com.example.teamlist.repository.PlayerRepo
import com.example.teamlist.repository.Repo
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class ManyPlayersViewModel(
//    private val cloudRepository: CloudDataRepository
private val mRepository: PlayerRepo
) {
    val job = Job()
    val ioScope = CoroutineScope(Dispatchers.IO + job)
    val uiScope = CoroutineScope(Dispatchers.Main + job)
    fun getPlayersForList(teamId: Int, listener: ManyPlayersVMInterface){
        ioScope.launch {
        lateinit var initialData: List<Player>
        mRepository.getPlayersDataByTeamId(teamId, object:Repo.Listener<List<Player>>{
            override fun <T> onResult(data: T) {
                initialData = data as List<Player>
                var listToDisplay: ArrayList<ListPlayerViewModel> = ArrayList()
                if (initialData != null) {
                    for (data in initialData) {
                        listToDisplay.add(ListPlayerViewModel(data.name, data.position, data.playerId, data.image))
                    }
                }
                uiScope.launch { listener.onDataReady(listToDisplay)}
            }
        })
    }
    }
    interface ManyPlayersVMInterface{
        fun onDataReady(listPlayerViewModel: List<ListPlayerViewModel>)
    }
}