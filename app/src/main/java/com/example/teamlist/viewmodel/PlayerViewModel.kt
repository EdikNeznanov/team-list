package com.example.teamlist.viewmodel

import androidx.databinding.ObservableField
import com.example.teamlist.model.Player
import com.example.teamlist.repository.PlayerRepo
import com.example.teamlist.repository.Repo
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class PlayerViewModel(val mTeamId: Int, val mPlayerId: Int, val mRepository: PlayerRepo) {
    val playerName = ObservableField<String>()
    val playerDescription = ObservableField<String>()
    val playerId = ObservableField<Int>()
    val playerPhotoURL = ObservableField<String>()
    val job = Job()
    val ioScope = CoroutineScope(Dispatchers.IO + job)
    val uiScope = CoroutineScope(Dispatchers.Main + job)

    fun getPlayersData(listener: PlayerVMListener) {
        ioScope.launch {
        mRepository.getPlayerDataById(mTeamId, mPlayerId, object : Repo.Listener<Player> {
            override fun <T> onResult(data: T) {
                data as Player
                playerName.set(data.name)
                playerDescription.set(data.story)
                playerId.set(data.playerId)
                playerPhotoURL.set(data.image)
                uiScope.launch {
                listener.onDataReady(data as Player)
            }
            }
        })
    } }

    interface PlayerVMListener{
        fun onDataReady(player: Player)
    }
}