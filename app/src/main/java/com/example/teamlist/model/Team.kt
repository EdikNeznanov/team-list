package com.example.teamlist.model


data class Team(val teamName: String, val teamStory: String, val teamLogoURL: String, val teamId: Int) {

}