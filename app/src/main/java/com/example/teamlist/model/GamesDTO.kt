package com.example.teamlist.model

import com.google.gson.annotations.SerializedName

data class GamesDTO(
    @SerializedName("results")
    val games: List<GameDTO>
)