package com.example.teamlist.model

import com.google.gson.annotations.SerializedName

data class GameDTO(
    @SerializedName("strHomeTeam")
    val strHomeTeam: String,
    @SerializedName("strAwayTeam")
    val strAwayTeam: String,
    @SerializedName("dateEvent")
    val dateEvent: String
)