package com.example.teamlist.model

import com.google.gson.annotations.SerializedName

data class PlayerDTO(
    @SerializedName("idPlayer")
    val idPlayer: Int,
    @SerializedName("idTeam")
    val idTeam: Int,
    @SerializedName("strPlayer")
    val strPlayer: String,
    @SerializedName("dateBorn")
    val dateBorn: String,
    @SerializedName("strDescriptionEN")
    val strDescriptionEN: String,
    @SerializedName("strPosition")
    val strPosition: String,
    @SerializedName("strHeight")
    val strHeight: String,
    @SerializedName("strWeight")
    val strWeight: String,
    @SerializedName("strThumb")
    val strThumb: String
)