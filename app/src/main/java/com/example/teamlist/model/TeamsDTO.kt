package com.example.teamlist.model

data class TeamsDTO (val teams: List<TeamDTO>)
