package com.example.teamlist.model

import com.google.gson.annotations.SerializedName

data class TeamDTO(@SerializedName("idTeam")
                   val idTeam: Int,
                   @SerializedName("strTeam")
                   val strTeam: String,
                   @SerializedName("strDescriptionEN")
                   val strDescriptionEN: String,
                   @SerializedName("strTeamLogo")
                   val strTeamLogo: String)