package com.example.teamlist.model

data class Player(
    val name: String,
    val age: Int,
    val height: Int,
    val weight: Int,
    val story: String,
    val image: String,
    val position: String,
    val playerId: Int,
    val teamId: Int
)