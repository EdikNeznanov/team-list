package com.example.teamlist.model

data class Game(val teamHome: String, val teamAway: String, val date: String) {
}