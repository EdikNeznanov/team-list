package com.example.teamlist.adapter

import android.content.Context
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.teamlist.R
import com.example.teamlist.databinding.PlayerItemBinding
import com.example.teamlist.viewmodel.ListPlayerViewModel
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target
import java.lang.Exception

class PlayersAdapter(
    private var list: List<ListPlayerViewModel>,
    private val mClickHandler: RecyclerViewAdapterOnClickHandler
) : RecyclerView.Adapter<PlayersAdapter.PlayerViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PlayerViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding: PlayerItemBinding = DataBindingUtil.inflate(inflater, R.layout.player_item, parent, false)
        return PlayerViewHolder(parent.context, binding)
    }

    interface RecyclerViewAdapterOnClickHandler {
        fun onClick(position: Int, playerID: Int, playerName: String)
    }
    fun setData(data: List<ListPlayerViewModel>){
        list = data
    }
    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: PlayerViewHolder, position: Int) {
        val item: ListPlayerViewModel = list[position]
        holder.bind(item)
    }

    inner class PlayerViewHolder(private val context: Context, private var binding: PlayerItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: ListPlayerViewModel) {
            binding.player = item
//            Picasso.get().load(item.photoURL).into(object : Target {
//                override fun onPrepareLoad(placeHolderDrawable: Drawable?) {
//                }
//
//                override fun onBitmapFailed(e: Exception?, errorDrawable: Drawable?) {
//                }
//
//                override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {
//                    binding.playerphoto.background = BitmapDrawable(bitmap)
//                }
//
//            })
            Picasso.get().load(item.photoURL).into(binding.playerphoto)
            binding.root.setOnClickListener { mClickHandler.onClick(adapterPosition, item.playerId, item.playerName) }
            binding.executePendingBindings()
        }
    }
}