package com.example.teamlist.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.teamlist.R
import com.example.teamlist.databinding.TeamItemBinding
import com.example.teamlist.viewmodel.TeamViewModel
import com.squareup.picasso.Picasso

class TeamsAdapter (
    private var list: List<TeamViewModel>,
    private val mClickHandler: TeamsAdapterOnClickHandler
) : RecyclerView.Adapter<TeamsAdapter.TeamViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TeamViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding: TeamItemBinding = DataBindingUtil.inflate(inflater, R.layout.team_item, parent,false)
        return TeamViewHolder(binding)
    }
interface TeamsAdapterOnClickHandler{
    fun onClick(position: Int, teamId: Int, teamName: String, teamLogo: String)

}
 fun setData(data: List<TeamViewModel>){
     list = data
 }
    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: TeamViewHolder, position: Int) {
        val item: TeamViewModel = list[position]
        holder.bind(item)
    }

    inner class TeamViewHolder(private var binding: TeamItemBinding): RecyclerView.ViewHolder(binding.root){

        fun bind(item: TeamViewModel) {
            binding.team = item
//            binding.imageUrl = item.logo
            binding.root.setOnClickListener { mClickHandler.onClick(adapterPosition, item.teamId, item.teamName, item.logo) }
            //val teamImg : ImageView = binding.teamLogo
            Picasso.get().load(item.logo).into(binding.teamLogo)
            binding.executePendingBindings()
        }
    }
}
