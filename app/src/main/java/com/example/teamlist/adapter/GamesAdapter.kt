package com.example.teamlist.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.teamlist.R
import com.example.teamlist.databinding.GameItemBinding
import com.example.teamlist.viewmodel.GameViewModel
import com.example.teamlist.viewmodel.TeamViewModel

class GamesAdapter (
    private var list: List<GameViewModel>

) : RecyclerView.Adapter<GamesAdapter.GameViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GameViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding: GameItemBinding = DataBindingUtil.inflate(inflater, R.layout.game_item, parent, false)
        return GameViewHolder(binding)
    }

    fun setData(data: List<GameViewModel>){
        list = data
    }
    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: GameViewHolder, position: Int) {
        val item: GameViewModel = list[position]
        holder.bind(item)
    }

    class GameViewHolder(private var binding: GameItemBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(item: GameViewModel) {
            binding.game = item
            binding.executePendingBindings()
        }
    }
}