package com.example.teamlist.adapter

import android.util.Log
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter

class GamesPlayersVPAdapter(fragmentManager: FragmentManager, private val mFragments: List<Fragment>) :
    FragmentPagerAdapter(fragmentManager) {


    override fun getItem(position: Int): Fragment {
return mFragments[position]
    }

    override fun getCount(): Int {
return mFragments.size
    }

    override fun getPageTitle(position: Int): CharSequence {
    when (mFragments[position].javaClass.simpleName){
        "PlayersFragment" -> return "Players"
        "GamesFragment" -> return "Games"
        else -> return "New Fragment"
    }
        //return mFragments[position].javaClass.simpleName
    }
}
