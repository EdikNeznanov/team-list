package com.example.teamlist.database

import androidx.room.*


@Dao
interface PlayerDAO {
    @Query("SELECT  * FROM players WHERE teamId = :teamID")
    fun getPlayers(teamID: Int): List<PlayerEntity>

    @Query("SELECT * FROM players WHERE playerId = :id")
    fun getPlayerById(id: Int): PlayerEntity

    @Update
    fun addUPlayer(player : PlayerEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addPlayer(player: PlayerEntity)

    @Delete
    fun deletePlayer(player: PlayerEntity)
}