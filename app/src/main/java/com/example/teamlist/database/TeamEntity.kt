package com.example.teamlist.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "teams")
data class TeamEntity(
    @PrimaryKey val id: Int?,
    @ColumnInfo(name = "teamId")val teamId: Int,
    @ColumnInfo(name = "teamname")val teamName: String,
    @ColumnInfo(name = "teamdescription")val teamDescription: String,
    @ColumnInfo(name = "teamphoto")val teamPhoto: String
)