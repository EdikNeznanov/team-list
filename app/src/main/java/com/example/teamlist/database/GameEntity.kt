package com.example.teamlist.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "events")
data class GameEntity(
    @PrimaryKey val id: Int?,
    @ColumnInfo(name = "team1Id")val teamId: Int?,
    @ColumnInfo(name = "team2name")val awayTeamName: String,
    @ColumnInfo(name = "team1name")val homeTeamName: String,
    @ColumnInfo(name = "teamphoto")val gameDate: String
//FIXME: connections in between tables
)

