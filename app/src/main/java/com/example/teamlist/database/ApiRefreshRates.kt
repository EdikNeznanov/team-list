package com.example.teamlist.database

enum class ApiRefreshRates(val ID: String, val renewTimeMS: Long) {
    Team("Team", 3600000), //3600sec
    Game("Game", 900000), // 900 sec
    Player("Player", 1800000); //1800sec

}