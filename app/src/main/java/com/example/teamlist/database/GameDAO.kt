package com.example.teamlist.database

import androidx.room.*


@Dao
interface GameDAO {
    @Query("SELECT  * FROM events")
    fun getGames(): List<GameEntity>

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun addUGame(game : GameEntity)

    @Insert
    fun addGame(game: GameEntity)

    @Delete
    fun deleteGame(game: GameEntity)
}