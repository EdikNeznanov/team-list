package com.example.teamlist.database

import android.provider.BaseColumns

class DbContract {
    object TeamListEntry : BaseColumns {
        const val TEAM_TABLE_NAME = "teams"
        const val COLUMN_NAME_TEAMID = "teamId"
        const val COLUMN_NAME_TEAM_NAME = "teamname"
        const val COLUMN_NAME_TEAM_DESCRIPTION = "teamdescription"
        const val COLUMN_NAME_TEAM_PHOTOURL = "teamphoto"
    }
    object EventListEntry : BaseColumns {
        const val EVENT_TABLE_NAME = "events"
        const val COLUMN_NAME_TEAMID = "team1Id"
        const val COLUMN_NAME_AWAY_TEAM_NAME = "team2name"
        const val COLUMN_NAME_HOME_TEAM_NAME = "team1name"
        const val COLUMN_NAME_GAME_DATE = "gamedate"
    }
    object PlayerListEntry : BaseColumns {
        const val PLAYER_TABLE_NAME = "players"
        const val COLUMN_NAME_PLAYER_ID = "playerId"
        const val COLUMN_NAME_TEAM_ID = "teamId"
        const val COLUMN_NAME_PLAYER_NAME = "playerName"
        const val COLUMN_NAME_PLAYER_AGE = "playerAge"
        const val COLUMN_NAME_PLAYER_HEIGHT = "playerHeight"
        const val COLUMN_NAME_PLAYER_WEIGHT = "playerWeight"
        const val COLUMN_NAME_PLAYER_DESCRIPTION = "playerDescription"
        const val COLUMN_NAME_PLAYER_PHOTOURL = "playerPhoto"
        const val COLUMN_NAME_PLAYER_POSITION = "playerPosition"

    }
}