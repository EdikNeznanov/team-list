package com.example.teamlist.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "players")
data class PlayerEntity(
    @PrimaryKey val uid: Int?,
    @ColumnInfo(name = "playerId") val playerId: Int?,
    @ColumnInfo(name = "teamId") val teamId: Int?,
    @ColumnInfo(name = "playerName") val playerName: String?,
    @ColumnInfo(name = "playerAge") val playerAge: Int?,
    @ColumnInfo(name = "playerHeight") val playerHeight: Int?,
    @ColumnInfo(name = "playerWeight") val playerWeight: Int?,
    @ColumnInfo(name = "playerDescription") val playerDescription: String?,
    @ColumnInfo(name = "playerPhoto") val playerPhoto: String?,
    @ColumnInfo(name = "playerPosition") val playerPosition: String?
    )