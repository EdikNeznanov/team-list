package com.example.teamlist.database

import androidx.room.*
import com.example.teamlist.database.DbContract.TeamListEntry.TEAM_TABLE_NAME
import com.example.teamlist.model.Team
import com.example.teamlist.repository.Repo

@Dao
interface TeamDAO {

    @Query("SELECT * FROM $TEAM_TABLE_NAME")
    fun getTeams(): List<TeamEntity>
//    listener: Repo.Listener<Team>
    @Update
    fun addUTeam(team : TeamEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addTeam(team: TeamEntity)

    @Delete
    fun deleteTeam(team: TeamEntity)
}