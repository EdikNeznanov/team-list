package com.example.teamlist.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import javax.inject.Singleton

const val DATABASE_NAME = "Teamlist.db"

@Singleton
@Database(entities = arrayOf(TeamEntity::class, GameEntity::class, PlayerEntity::class), version = 4, exportSchema = false)
abstract class BasketballDB: RoomDatabase() {

    companion object {
        private var instance: BasketballDB? = null
        var TEST_MODE = false

        fun getInstance(appContext: Context): BasketballDB {
            if (instance == null) {
                if(TEST_MODE){
                    instance =Room.inMemoryDatabaseBuilder(appContext, BasketballDB::class.java).allowMainThreadQueries().build()
                }
                else {
                    instance =
                        Room.databaseBuilder(appContext.applicationContext, BasketballDB::class.java, DATABASE_NAME)
                            .fallbackToDestructiveMigration().build()
                } }

            return instance!!
        }
    }
    abstract fun teamDao(): TeamDAO
    abstract fun gameDao(): GameDAO
    abstract fun playerDao(): PlayerDAO
}