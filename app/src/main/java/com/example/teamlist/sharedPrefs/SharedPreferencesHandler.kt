package com.example.teamlist.sharedPrefs

import android.content.SharedPreferences


class SharedPreferencesHandler(val sharedPrefs: SharedPreferences) {
    companion object {
        private var instance: SharedPreferencesHandler? = null

        fun getInstance(sharedPrefs: SharedPreferences): SharedPreferencesHandler {
            if (instance == null) {
                instance = SharedPreferencesHandler(sharedPrefs)
            }

            return instance!!
        }
    }

}