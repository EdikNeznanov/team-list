package com.example.teamlist

//import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.runner.AndroidJUnit4
import androidx.viewpager.widget.ViewPager
import org.hamcrest.Matchers.allOf
import org.hamcrest.Matchers.instanceOf
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
//@Config
class UITest {
    @get:Rule
    public var activityScenarioRule = ActivityScenarioRule<MainActivity>(MainActivity::class.java)



    @Test
    fun teamnameMatchinTitleText() {
        onView(withId(R.id.teamListRecycler))
            .perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(1, click()))
        //onView(withId(R.id.gamesTeamsToolbar)).check(matches(hasDescendant(withText("Team List"))))
        //onView(isAssignableFrom(Toolbar::class.java)).check(matches(withToolbarTitle("Toronto Velociraptors")))
        onView(allOf(instanceOf(AppCompatTextView::class.java), withParent(withId(R.id.gamesTeamsToolbar))))
            .check(matches(withText("Toronto Velociraptors")))
    }

    @Test
    fun viewPagerGamesTabNaming() {
        onView(withId(R.id.teamListRecycler))
            .perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(1, click()))
        onView(allOf(instanceOf(AppCompatTextView::class.java), isDescendantOfA(withId(R.id.tl_tabs_container)), isSelected()))
            //withParent(withId(R.id.tl_tabs_container))))
            .check(matches(withText("Games")))
    }

    @Test
    fun viewPagerPlayersTabNaming() {
        onView(withId(R.id.teamListRecycler))
            .perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(1, click()))
        onView(allOf(instanceOf(ViewPager::class.java), withId(R.id.viewPager))).perform(swipeLeft())
        onView(allOf(instanceOf(AppCompatTextView::class.java), isDescendantOfA(withId(R.id.tl_tabs_container)), isSelected()))
            //withParent(withId(R.id.tl_tabs_container))))
            .check(matches(withText("Players")))
    }

}
