package com.example.teamlist

import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import com.example.teamlist.database.BasketballDB
import com.example.teamlist.database.GameEntity
import com.example.teamlist.database.PlayerEntity
import com.example.teamlist.model.Game
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class DatabaseTests {
    private var basketballDB: BasketballDB? = null

    @Before
    fun setup() {
        BasketballDB.TEST_MODE = true
        basketballDB = BasketballDB.getInstance(InstrumentationRegistry.getInstrumentation().targetContext)
    }

    @Test
    fun should_Insert_Game_Item() {
        val game = GameEntity(null, 158254, "teamName1", "homeTeam", "May 22nd")
        basketballDB?.gameDao()!!.addGame(game)
        val gameTest = basketballDB?.gameDao()!!.getGames()
        Assert.assertEquals(game.homeTeamName, gameTest[0].homeTeamName)
    }

    @Test
    fun should_Insert_And_Find_Player_Item_by_Id() {
        val player = PlayerEntity(null, 158254, 546546, "TeamPlayer", 22, 150, 150, "description", "someurl", "center")
        basketballDB?.playerDao()!!.addPlayer(player)
        val player2 = PlayerEntity(null, 1584, 546546, "TeamPlayer", 22, 150, 150, "description", "someurl", "center")
        basketballDB?.playerDao()!!.addPlayer(player2)
        val playerTest = basketballDB?.playerDao()!!.getPlayerById(player.playerId!!)
        Assert.assertEquals(player.playerId, playerTest.playerId)
    }

}